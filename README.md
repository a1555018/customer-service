nano docker-compose.yml

nano dockerfile

nano deploy.yml

nano requirements.txt

nano app.py

nano inventory.ini

docker-compose up -d

docker ps

docker-compose up --build

Check Container Logs: docker-compose logs -f web

This command will show you the logs for the customer-service_web_1 container: docker logs customer-service_web_1

Restart the Docker Compose services: docker-compose down  
                                     docker-compose up -d

This is the command used to run Ansible playbooks: ansible-playbook -i inventory.ini deploy.yml


---------------------------------------------------------------------------------------------------


# To test your web application deployed using Docker and managed via an Ansible playbook, you can follow these steps:

If You are on a Local Machine:
1-Access via Web Browser:

-If the application is running on your local machine, you can simply open a web browser and go to http://localhost:3000 or http://127.0.0.1:3000. This should load your application's main page.

2-Use curl or wget:

-You can also use command-line tools like curl or wget to fetch the application's main page. Run curl http://localhost:3000 or wget http://localhost:3000 from the terminal.

till example:  

server3@ubuntuserver3:~/customer-service$ curl http://localhost:3000
Hello, World!



If Deployed on a Remote Server:

1-Access via Web Browser:

-If your application is hosted on a remote server, you'll need to know the server's IP address or domain name. Open a web browser and navigate to http://<your_server_ip>:3000 or http://<your_domain_name>:3000.


2-SSH Port Forwarding (If Necessary):

-If the application is not exposed to the internet or is behind a firewall, you might need to use SSH port forwarding to access it. Run ssh -L 3000:localhost:3000 user@remote_server from your local machine, and then access http://localhost:3000 in your browser.



                                
